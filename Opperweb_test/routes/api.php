<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



// CRUD CATEGORIAS
Route::group([
    'prefix' => 'category'
], function ($router) {
    Route::post('create', 'CategoryController@create');
    Route::get('show', 'CategoryController@show');
    Route::get('showId/{id}', 'CategoryController@showId');
    Route::put('update/{id}', 'CategoryController@update');
    Route::put('delete/{id}', 'CategoryController@delete');
});

// CRUD POSTS
Route::group([
    'prefix' => 'post'
], function ($router) {
    Route::post('create', 'PostController@create');
    Route::get('show', 'PostController@show');
    Route::get('showId/{id}', 'PostController@showId');
    Route::put('update/{id}', 'PostController@update');
    Route::put('delete/{id}', 'PostController@delete');
});

// CRUD COMENTARIOS
Route::group([
    'prefix' => 'comment'
], function ($router) {
    Route::post('create', 'CommentController@create');
    Route::get('show', 'CommentController@show');
    Route::get('showId/{id}', 'CommentController@showId');
    Route::put('update/{id}', 'CommentController@update');
    Route::put('delete/{id}', 'CommentController@delete');
});

