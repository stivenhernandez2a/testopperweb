<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required",
            "content" => "required",
            "category_id" => "integer|required",
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo title es obligatorio.',
            'content.required' => 'El campo content es obligatorio.',
            'category_id.required' => 'El campo category_id es obligatorio.',
            'category_id.integer' => 'El campo category_id es obligatorio.'
        ];
    }
}
