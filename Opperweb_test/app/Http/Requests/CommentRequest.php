<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "content" => "required",
            "post_id" => "integer|required",
        ];
    }

    public function messages()
    {
        return [
            'content.required' => 'El campo content es obligatorio.',
            'post_id.required' => 'El campo category_id es obligatorio.',
            'post_id.integer' => 'El campo category_id es obligatorio.'
        ];
    }
}
