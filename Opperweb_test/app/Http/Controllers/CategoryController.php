<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\CategoryService;

use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function __construct(CategoryService $category_service)
    {
        $this->category_service = $category_service;
    }

    /**
     * Servicio: Creacion de categorías.
     */
    public function create(CategoryRequest $request)
    {
        return $categoria = $this->category_service->create($request);
    }

    /**
     * Servicio: Listar todas la categorías.
     */
    public function show()
    {
        return $categoria = $this->category_service->show();
    }

    /**
     * Servicio: Consultar una categoría.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showId($id)
    {
        return $categoria = $this->category_service->showId($id);
    }

    /**
     * Servicio: Actualizar categorías.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function update(CategoryRequest $request, $id)
    {
        return $categoria = $this->category_service->update($request, $id);
    }

    /**
     * Servicio: Eliminar Categorías.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function delete($id)
    {
        return $categoria = $this->category_service->delete($id);
    }
}
