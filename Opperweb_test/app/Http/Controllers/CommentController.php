<?php

namespace App\Http\Controllers;

use App\Services\CommentService;

use App\Http\Requests\CommentRequest;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct(CommentService $comment_service)
    {
        $this->comment_service = $comment_service;
    }

    /**
     * Servicio: Creacion de comentarios.
     * @return \Illuminate\Http\Response
     */
    public function create(CommentRequest $request)
    {
        return $comment = $this->comment_service->create($request);
    }

    /**
     * Servicio: Listar todas los comentarios con su respectivo post y categoria.
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return $comment = $this->comment_service->show();
    }

    /**
     * Servicio: Consultar un comentario con su respectivo post y categoria.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showId($id)
    {
        return $comment = $this->comment_service->showId($id);
    }

    /**
     * Servicio: Actualizar comentario.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function update(CommentRequest $request, $id)
    {
        return $comment = $this->comment_service->update($request, $id);
    }

    /**
     * Servicio: Eliminar comentario.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function delete($id)
    {
        return $comment = $this->comment_service->delete($id);
    }
}
