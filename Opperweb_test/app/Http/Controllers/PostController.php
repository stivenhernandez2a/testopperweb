<?php

namespace App\Http\Controllers;

use App\Services\PostService;

use App\Http\Requests\PostRequest;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct(PostService $post_service)
    {
        $this->post_service = $post_service;
    }

    /**
     * Servicio: Creacion de posts.
     * @return \Illuminate\Http\Response
     */
    public function create(PostRequest $request)
    {
        return $post = $this->post_service->create($request);
    }

    /**
     * Servicio: Listar todas los post con su respectiva categoria.
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return $post = $this->post_service->show();
    }

    /**
     * Servicio: Consultar un post con su respectiva categoria.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function showId($id)
    {
        return $post = $this->post_service->showId($id);
    }

    /**
     * Servicio: Actualizar post.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function update(PostRequest $request, $id)
    {
        return $post = $this->post_service->update($request, $id);
    }

    /**
     * Servicio: Eliminar post.
     * @return \Illuminate\Http\Response
     * @param  int  $id
     */
    public function delete($id)
    {
        return $post = $this->post_service->delete($id);
    }
}
