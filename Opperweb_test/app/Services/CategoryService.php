<?php

namespace App\Services;

use Log;
use Carbon\Carbon;

use App\Models\Category;

use Illuminate\Http\Response as HttpResponse;

class CategoryService
{
    /**
     * Crear categorías.
     */
    public function create($request)
    {
        $service_name = (__FUNCTION__);
        try {

            $category = new Category;
            $category->name = $request->name;
        
            if ($category->save()) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Categorías creada con éxito.',
                    'data' => $category
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se pudo crear la categoría',
                    'data' => $category
                ], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CategoryController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de crear la categoria, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Listar Categorías.
     */
    public function show()
    {
        $service_name = (__FUNCTION__);
        try {

            $categories = Category::get();
            if (count($categories) > 0) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Categorías consultadas con éxito.',
                    'data' => $categories
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontraron categorías.',
                    'data' => $categories
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CategoryController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar las categorias, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Buscar categorías.
     */
    public function showId($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $category = Category::where('id', $id)->first();
            if ($category) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Categoría consultada con éxito.',
                    'data' => $category
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro la categoría.',
                    'data' => $category
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CategoryController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar la categoria, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Actualizar categorías.
     */
    public function update($request, $id)
    {
        $service_name = (__FUNCTION__);
        try {

            $category = Category::where('id', $id)->first();
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro la categoría.',
                    'data' => $category
                ], HttpResponse::HTTP_NOT_FOUND);
            } else {
                $category->name = $request->name;
                if ($category->save()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Categoría actualizada con éxito.',
                        'data' => $category
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo actualizar la categoría',
                        'data' => $category
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CategoryController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de actualizar la categoria, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Eliminar Categorías.
     */
    public function delete($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $category = Category::where('id', $id)->first();
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro la categoría.',
                    'data' => $category
                ], HttpResponse::HTTP_NOT_FOUND);
            } else {
                if ($category->delete()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Categoría Eliminada.',
                        'data' => []
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo eliminar la categoría',
                        'data' => null
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }
            }
            
        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CategoryController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de eliminar la categoria, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
}
