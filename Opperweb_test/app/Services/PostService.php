<?php

namespace App\Services;

use Log;
use Carbon\Carbon;

use App\Models\Post;
use App\Models\Category;

use Illuminate\Http\Response as HttpResponse;

class PostService
{

    /**
     * Crear posts.
     * Si ID de la categoria no existe no permitira crear el post.
     */
    public function create($request)
    {
        $service_name = (__FUNCTION__);
        try {
            
            $category = Category::where('id', $request->category_id)->first();
            if (!$category) {
                return response()->json([
                    'success' => false,
                    'messages' => 'El ID de la categoria no existe.',
                    'data' => $category
                ], HttpResponse::HTTP_NOT_FOUND);
            }

            $post = new Post;
            $post->title = $request->title;
            $post->content = $request->content;
            $post->category_id = $request->category_id;
        
            if ($post->save()) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Post creado con éxito.',
                    'data' => $post
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se pudo crear el post',
                    'data' => $post
                ], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en PostController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de crear el post, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Listar posts con su respectiva categoria.
     */
    public function show()
    {
        $service_name = (__FUNCTION__);
        try {

            $post = Post::select(
                'posts.id AS post_id',
                'posts.title AS post_title',
                'posts.content AS post_content',
                'posts.category_id',
                'categories.name AS category',
                'posts.created_at',
                'posts.updated_at'
            )
            ->join('categories', 'posts.category_id', 'categories.id')
            ->get();

            if (count($post) > 0) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Posts consultadas con éxito.',
                    'data' => $post
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontraron post.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en PostController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar los posts, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Buscar post con su respectiva categoria.
     */
    public function showId($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $post = Post::select(
                'posts.id AS post_id',
                'posts.title AS post_title',
                'posts.content AS post_content',
                'posts.category_id',
                'categories.name AS category',
                'posts.created_at',
                'posts.updated_at'
            )
            ->join('categories', 'posts.category_id', 'categories.id')
            ->where('posts.id', $id)->first();

            if ($post) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Post consultado con éxito.',
                    'data' => $post
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro el post.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en PostController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar el post, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Actualizar categorías.
     * Si ID de la categoria no existe no permitira actualizar el post.
     */
    public function update($request, $id)
    {
        $service_name = (__FUNCTION__);
        try {

            $post = Post::where('id', $id)->first();
            if (!$post) {

                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro el post.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);

            } else {

                $category = Category::where('id', $request->category_id)->first();
                if (!$category) {
                    return response()->json([
                        'success' => false,
                        'messages' => 'El ID de la categoria no existe.',
                        'data' => $category
                    ], HttpResponse::HTTP_NOT_FOUND);
                }

                $post->title = $request->title;
                $post->content = $request->content;
                $post->category_id = $request->category_id;

                if ($post->save()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Post actualizado con éxito.',
                        'data' => $post
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo actualizar el post.',
                        'data' => $post
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }

            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en PostController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de actualizar el post, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Eliminar Categorías.
     */
    public function delete($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $post = Post::where('id', $id)->first();
            if (!$post) {

                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro la post.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);

            } else {

                if ($post->delete()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Post Eliminado.',
                        'data' => []
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo eliminar el post.',
                        'data' => null
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }

            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en PostController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de eliminar el post, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
}
