<?php

namespace App\Services;

use Log;
use Carbon\Carbon;

use App\Models\Post;
use App\Models\Comment;

use Illuminate\Http\Response as HttpResponse;

class CommentService
{
    /**
     * Crear comentarios.
     * Si ID del post no existe no permitira crear el comentario.
     */
    public function create($request)
    {
        $service_name = (__FUNCTION__);
        try {

            $post = Post::where('id', $request->post_id)->first();
            if (!$post) {
                return response()->json([
                    'success' => false,
                    'messages' => 'El ID deL post no existe.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);
            }

            $comment = new Comment;
            $comment->post_id = $request->post_id;
            $comment->content = $request->content;
        
            if ($comment->save()) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Comentario creado con éxito.',
                    'data' => $comment
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se pudo crear el comentario.',
                    'data' => $comment
                ], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CommentController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de crear el comentario, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Listar comentarios.
     */
    public function show()
    {
        $service_name = (__FUNCTION__);
        try {

            $comment = Comment::select(
                'comments.id AS comment_id',
                'comments.content AS comment_content',
                'posts.id AS post_id',
                'posts.title AS post_title',
                'posts.content AS post_content',
                'categories.id AS category_id',
                'categories.name AS category_name',
                'comments.created_at',
                'comments.updated_at'
            )
            ->join('posts', 'comments.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->get();

            if (count($comment) > 0) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Comentarios consultados con éxito.',
                    'data' => $comment
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontraron comentarios.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CommentController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar los comentarios, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Buscar comentarios.
     */
    public function showId($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $comment = Comment::select(
                'comments.id AS comment_id',
                'comments.content AS comment_content',
                'posts.id AS post_id',
                'posts.title AS post_title',
                'posts.content AS post_content',
                'categories.id AS category_id',
                'categories.name AS category_name',
                'comments.created_at',
                'comments.updated_at'
            )
            ->join('posts', 'comments.post_id', 'posts.id')
            ->join('categories', 'posts.category_id', 'categories.id')
            ->where('comments.id', $id)
            ->first();

            if ($comment) {
                return response()->json([
                    'success' => true,
                    'messages' => 'Comentario consultado con éxito.',
                    'data' => $comment
                ], HttpResponse::HTTP_OK);
            } else {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro el comentario.',
                    'data' => $comment
                ], HttpResponse::HTTP_NOT_FOUND);
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CommentController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de consultar el comentario, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Actualizar comentarios.
     * Si ID del post no existe no permitira actualizar el comentario.
     */
    public function update($request, $id)
    {
        $service_name = (__FUNCTION__);
        try {

            $commnet = Comment::where('id', $id)->first();
            if (!$commnet) {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro el comentario.',
                    'data' => $commnet
                ], HttpResponse::HTTP_NOT_FOUND);
            } else {
                $post = Post::where('id', $request->post_id)->first();
                if (!$post) {
                    return response()->json([
                        'success' => false,
                        'messages' => 'El ID del post no existe.',
                        'data' => $post
                    ], HttpResponse::HTTP_NOT_FOUND);
                }

                $commnet->content = $request->content;
                $commnet->post_id = $request->post_id;

                if ($commnet->save()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Comentario actualizado con éxito.',
                        'data' => $commnet
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo actualizar el comentario',
                        'data' => $commnet
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }
            }
            
        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CommentController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de Actualizar el comentario, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Eliminar comentarios.
     */
    public function delete($id)
    {
        $service_name = (__FUNCTION__);
        try {

            $comment = Comment::where('id', $id)->first();
            if (!$comment) {
                return response()->json([
                    'success' => false,
                    'messages' => 'No se encontro el comentario.',
                    'data' => $post
                ], HttpResponse::HTTP_NOT_FOUND);
            } else {
                if ($comment->delete()) {
                    return response()->json([
                        'success' => true,
                        'messages' => 'Comentario Eliminado.',
                        'data' => []
                    ], HttpResponse::HTTP_OK);
                } else {
                    return response()->json([
                        'success' => false,
                        'messages' => 'No se pudo eliminar el comentario.',
                        'data' => null
                    ], HttpResponse::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            $time = Carbon::now()->timestamp;
            Log::info($time . ' Error en CommentController@'.$service_name);
            Log::info($e);
            
            return response()->json([
                'success' => false,
                'messages' => 'Ocurrió un error al momento de eliminar el comentario, Comuníquese con el administrador.',
                'data' => 'Error: '.$time
            ], HttpResponse::HTTP_BAD_REQUEST);
        }
    }
}
