<?php

namespace Database\Factories;

use app\Models\Post;
use app\Models\Comment;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $topId = Post::min('id');
        $lastId = Post::max('id');

        return [
            'post_id' => $this->faker->numberBetween($topId, $lastId),
            'content' => $this->faker->sentence(5),
        ];
    }
}
