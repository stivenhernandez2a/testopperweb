<?php

namespace Database\Factories;

use app\Models\Post;
use app\Models\Category;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $topId = Category::min('id');
        $lastId = Category::max('id');

        return [
            'category_id' => $this->faker->numberBetween($topId, $lastId),
            'title' => $this->faker->word,
            'content' => $this->faker->sentence(2),
        ];
    }
}
