1. Instrucciones Back
	1.1. Clonar proyecto desde nuestra consola, ejecutando el comando “git clone” junto a la url del repositorio.
	1.2. Ejecutar el comando “composer install”.
	1.3. Configurar archivo “.env” (la configuracion la encontraremos en el archivo llamado “.env.example”).
	1.4. Si se genera un error en el paso anterior indicando problemas con el límite de memoria usado (Allowed memory size of 1610612736 bytes exhausted),  se debe buscar la línea “memory_limit=” en el archivo “php.ini” y reemplazar su valor por “-1”.
	1.5. Ejecutamos el comando “php artisan serve” para levantar nuestro proyecto el cual nos indicara para que ruta y puerto donde se encentra corriendo.

2. Base de datos.
	2.1. Crear una base de datos llamada "prueba_opperweb" en nuestro gestor de base de datos.
	2.2. Ubicados en la raíz del proyecto ejecutamos el comando php artisan migrate:refresh --seed desde nuestro terminal.

3. Postman.
	3.1. Importar la colección JSON de Postman, la cual se encuentra anexada en el correo con los demás anexos de la prueba. La colección ya contiene todas las apis usadas actualmente en el proyecto.
	3.2. Finalmente se debe crear una variable en la opción “manage environments” con la dirección donde tenemos configurado nuestro localhost. “http://127.0.0.1:8000/api/” el cual pertenece a nuestro entorno local.
	3.3. Para realizar pruebas se debe ejecutar el comando “php artisan serve” en la raíz del proyecto.